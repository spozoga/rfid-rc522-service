angular.module('app', [])
    .controller('RfidController', ['io', '$rootScope', function(io, $rootScope){
        var that = this;

        that.list = [];
        that.token = "*";
        that.started = false;

        that.start = function () {
            var socket = io.start(that.token);

            socket.on('tap', function(data){
                that.list.push({
                    rfid: data.rfid,
                    date: new Date()
                });
                $rootScope.$apply();
            });

            that.started = true;
        };
    }])
    .service('io', [function () {
        var api = {};

        api.start = function (token) {
            return io({
                query: 'token=' + token
            });
        };

        return api;
    }]);