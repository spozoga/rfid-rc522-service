module.exports = function (config) {
    var app = require('express')(),
        server = require('http').Server(app),
        io = require('socket.io')(server),

        logger = require("./modules/logger"),
        auth = require("./modules/auth")(config, logger, app, io),
        rest = require("./modules/rest")(config, logger, app, io, auth),
        socket = require("./modules/socket")(config, logger, app, io, auth),
        webDebug;

    if (config.webdebug == true) {
        webDebug = require("./modules/webdebug")(config, logger, app, io);
    }

    //public security
    io.use(function (socket, next) {
        if (auth.auth(socket)) {
            logger.log("Login success");
            next();
        } else {
            logger.error("Authentication error");
            next(new Error('Authentication error'));
        }
    });

    // allow origin public
    io.set('origins', '*:*');

    app.use(function (req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "X-Requested-With");
        res.header("Access-Control-Allow-Headers", "Content-Type");
        res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
        next();
    });

    server.listen(config.port);
    logger.log("echo server started", [config]);
};
