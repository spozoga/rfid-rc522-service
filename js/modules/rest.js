var rfidUnit = require("../lib/rfidUnit"),
    historyList = [];

rfidUnit.addListener(function (rfid) {
    historyList.push({
        rfid: rfid,
        date: new Date()
    })
});

module.error = function (logger, res, msg) {
    logger.error(msg);
    res.json({
        status: "error",
        msg: msg
    }, 403);
};

module.checkPermission = function (auth, logger, res, token) {
    if (!token) {
        module.error(logger, res, "No set token");
        return false;
    }

    if (!auth.isReadToken(token)) {
        module.error(logger, res, "Token " + token + " doesn't have access permission");
        return false;
    }

    return true;
};

module.exports = function (config, logger, app, io, auth) {

    app.get('/rest/rfid', function (req, res) {
        var list = historyList,
            token = req.query.token,
            fromDate = req.query.fromDate ? new Date(req.query.fromDate) : null;

        if (!module.checkPermission(auth, logger, res, token)) return;

        if (fromDate) {
            list = list.filter(function (element) {
                return element.date >= fromDate
            });
        }

        res.json(list);
    });

    app.get('/rest/rfid/last', function (req, res) {
        var token = req.query.token;

        if (!module.checkPermission(auth, logger, res, token)) return;

        if (historyList.length == 0) {
            res.json(null);
            return;
        }

        res.json(historyList[historyList.length - 1]);
    });

    logger.log("rest module... started");
};
