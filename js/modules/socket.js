var rfidUnit = require("../lib/rfidUnit");

module.exports = function (config, logger, app, io, auth) {
    var emitTapAction = function (rfid) {
        logger.log("socket module: tap rfid", [rfid]);
        io.emit('tap', {rfid: rfid});
    };

    // emit
    rfidUnit.addListener(emitTapAction);

    // log
    io.on('connection', function (socket) {
        logger.log("socket module... connection");
    });

    logger.log("socket module... started");
};
