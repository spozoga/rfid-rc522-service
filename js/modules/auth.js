module.exports = function (config, logger, app, io) {
    var api = {};

    api.auth = function (socket) {
        var token = socket.handshake.query.token,
            row = api.getTokenRow(token);
        if (!row) return false;
        logger.log("auth", row);
        return true;
    };

    api.getTokenRow = function (token) {
        for (var i in config.tokens) {
            if (config.tokens[i].token == token) {
                return config.tokens[i];
            }
        }
        logger.error("No find token");
        return false;
    };

    api.isReadToken = function (token) {
        var row = api.getTokenRow(token);
        return row === false ? false : true;
    };

    logger.log("auth module... started");
    return api;
};
