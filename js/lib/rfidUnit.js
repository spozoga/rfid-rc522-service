var rc522 = require("rc522-rfid"),
    list = {},
    counter = 0,
    emiter;

module.exports.addListener = function (cb) {
    var key = "_" + (++counter);
    list[key] = cb;
};

module.emit = emiter = function (rfid) {
    for (var i in list) {
        list[i](rfid);
    }
};

rc522(function (rfid) {
    setTimeout(emiter.bind(undefined, rfid), 0);
});
